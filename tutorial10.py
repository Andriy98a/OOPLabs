# define the Vehicle class
class Vehicle:
    name = "fer"
    kind = "car1"
    color = "red"
    value = 60000.00
    def description(self):
        desc_str = "%s  is a %s %s worth $%.2f." % (self.name, self.color, self.kind, self.value)
        return desc_str
# your code goes here
car1=Vehicle()
car2=Vehicle()
car2.name="jump"
car2.kind="car2"
car2.color="red"
car2.value=10000.00
# test code
print(car1.description())

print(car2.description())